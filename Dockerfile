FROM klakegg/hugo:0.101.0-ext-ubuntu-ci as builder

WORKDIR /src

COPY . .

RUN hugo mod get -u 

ENV ENV=production

RUN hugo --minify --enableGitInfo

FROM lscr.io/linuxserver/nginx:latest

ENV PUID=1000
ENV PGID=1000
ENV TZ=America/Phoenix

EXPOSE 443
EXPOSE 80

COPY --from=builder /src/public /config/www

