+++
title = "Mantisd Cloud"
[data]
baseChartOn = 3
colors = ["#627c62", "#11819b", "#ef7f1a", "#4e1154"]
columnTitles = ["Section", "Status", "Author"]

+++
{{< block "grid-2" >}}
{{< column >}}

# Welcome to Mantisd Cloud!

This is the documentation page for all my home resources.  Feel free to open a [PR](https://gitlab.com/mantisd-cloud/mantisd-cloud-contianers), raise an [issue](https://gitlab.com/mantisd-cloud/mantisd-cloud-contianers/-/issues)(s) or request new feature(s).

{{< tip "warning" >}}
Under Construction{{< /tip >}}

<!-- {{< tip >}}
You can generate diagrams, flowcharts, and piecharts from text in a similar manner as markdown using [mermaid](./docs/compose/mermaid/).

Or, [generate graphs, charts](docs/compose/graphs-charts-tables/#show-a-pie-doughnut--bar-chart-at-once) and tables from a csv, ~~or a json~~ file.
{{< /tip >}} -->

{{< button "docs/" "Read the Docs" >}}
{{< /column >}}

{{< column >}}
![diy](/images/painting.jpg)
{{< /column >}}
{{< /block >}}