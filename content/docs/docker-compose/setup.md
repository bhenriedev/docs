---
title: "Setup"
weight: 2
description: >
  This page tells you how to get started with the Docker Compose.
---

### Prerequisites

First ensure that you have Docker & Docker Compose installed

```
sudo curl https://get.docker.com | bash
sudo usermod -aG docker $(whoami)
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo apt install -y docker-compose
```
Go into the directory of the docs/ and run the command

```
sudo docker-compose up
```

## Customize

The labels are left on if it is to be hosted in portainer under traefik load balancer. The current directory /docs is bound to the src directory in the container as well as the .git info. 

```
version: '3'
services:
  docs:
    image: klakegg/hugo:0.101.0-ext-ubuntu-ci
    container_name: docs
    command: server
    volumes:
      - "../docs:/src"
      - "../.git:/src/.git:ro"
    ports:
      - "1313:1313"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.docs.entrypoints=web,websecure"
      - "traefik.http.routers.docs.rule=Host(`docs.mantisd.cloud`)"
      - "traefik.http.routers.docs.tls=true"
      - "traefik.http.routers.docs.service=docs@docker"
      - "traefik.http.services.docs.loadbalancer.server.scheme=https"
      - "traefik.http.services.docs.loadbalancer.server.port=1313"
    environment:
      HUGO_BIND: 0.0.0.0
      GIT_DISCOVERY_ACROSS_FILESYSTEM: "true"
    command: ./entrypoint.sh
```

If you need to create a new site replace command: with

```
command: hugo new site ./
```
